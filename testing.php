<?php
require __DIR__.'/vendor/autoload.php';

$client = new \GuzzleHttp\Client([

    'base_url' => 'http://localhost:8000',
    'defaults' => [
        'exceptions' => false
    ]
]);

$data = array(
    'name' => "Bipolelm",
    'strength' => 10,
    'weapon_power' => 20
);

$id = rand(1,2);

// 1) POST to create a knight
$response = $client->post('knight', [
    'body' => json_encode($data)
]);

//$knightUrl = $response->getHeader('Location');

// 2) GET to fetch a knight
$response = $client->get('knight/'.$id);


echo $response;
echo "\n\n";