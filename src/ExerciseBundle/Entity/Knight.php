<?php

namespace ExerciseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ExerciseBundle\Model\FighterInterface;
use ExerciseBundle\Model\Human;

/**
 * Human
 *
 * @ORM\Table(name = "knight")
 * @ORM\Entity(repositoryClass="ExerciseBundle\Entity\KnightRepository")
 */
class Knight extends Human implements FighterInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="strength", type="integer", nullable=true)
     */
    private $strength;

    /**
     * @var integer
     *
     * @ORM\Column(name="weapon_power", type="integer", nullable=true)
     */
    private $weapon_power;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getWeaponPower()
    {
        return $this->weapon_power;
    }

    /**
     * @param int $weapon_power
     */
    public function setWeaponPower($weapon_power)
    {
        $this->weapon_power = $weapon_power;
    }

    /**
     * Calculate power
     *
     * @return integer
     */
    public function calculatePowerLevel()
    {
        $strength = $this->strength;
        $weaponPower = $this->weapon_power;
        $power = $weaponPower + $strength;

        return $power;
    }
}
