<?php

namespace ExerciseBundle\Controller;

use ExerciseBundle\Entity\Knight;
use ExerciseBundle\Form\KnightType;

use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;



class KnightController extends BaseController
{
    public function postKnightAction(Request $request)
    {

        $data = json_decode($request->getContent(), true);

        $knight = new Knight();
        $form = $this->createForm(new KnightType(), $knight);

        $form->submit($data);

        if (!$form->isValid()) {

//            print_r($form->getErrorsAsString());
            throw new HttpException(400, "New knight is not valid.");
        }

        $em = $this->getEntityManager();
        $em->persist($knight);
        $em->flush();

        $json = $this->serializeKnight($knight);

        $response = new JsonResponse($json, 201);

        return $response;

    }


    public function getKnightAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $knight = $em->getRepository('ExerciseBundle:Knight')->find($id);

        if(!$knight){
//            throw $this->createNotFoundException('Knight #'.$id.' not found.');
            throw new HttpException(404, "Knight #".$id." not found.");
        }

        $data = $this->serializeKnight($knight);

        $response =  new JsonResponse($data, 200, array('Location' => 'knight/'.$id));
        return $response;

    }


    public function getKnightsAction()
    {

        $em = $this->getEntityManager();
        $knights = $em->getRepository('ExerciseBundle:Knight')->findAll();


        //*************************************************************************************
        //********************************** Remarque ****************************************
        //**************************************************************************************

        //plus propre qu'un simple data[] car empêche les risques de JSON Hijacking
        
        $data = array('knights' => array());


        foreach ($knights as $knight) {
            $data['knights'][] = $this->serializeKnight($knight);
        }

        $response =  new JsonResponse($data, 200, array('Location' => 'knight'));
        return $response;

    }

    public function serializeKnight($knight)
    {
        return [
            'id' => $knight->getId(),
            'name' =>$knight->getName(),
            'weapon_power' => $knight->getWeaponPower(),
            'strength' => $knight->getStrength()
        ];

    }

}
