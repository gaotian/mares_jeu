<?php

namespace ExerciseBundle\Services;

class Arena
{
    public static function fight($knight, $knight2)
    {

        $power_knight  = $knight->calculatePowerLevel();
        $power_knight2  = $knight2->calculatePowerLevel();

        if ($power_knight > $power_knight2){
            $winner =  $knight;
            }
        else if($power_knight == $power_knight2){
            $winner = 0;
        }
        else{
            $winner = $knight2;
        }

        return $winner;
    }

}


