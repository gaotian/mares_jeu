<?php

namespace ExerciseBundle\Model;

/**
 * Interface FighterInterface
 *
 * Represents a Fighter.
 *
 * Each Fighter is ultimately identified by an ID
 * Each Fighter has a Power level depending of its stats
 *
 * @package ExerciseBundle\Model
 */
interface FighterInterface
{
    /**
     * Get the ID
     *
     * @return int
     */
    public function getId();


    /**
     * Calculate the Power Level of the Fighter
     *
     * @return int
     */
    public function calculatePowerLevel();

}
