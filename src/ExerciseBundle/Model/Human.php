<?php

namespace ExerciseBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Human
 *
 * To extend for the humans knight
 *
 * @package ExerciseBundle\Model
 * @ORM\MappedSuperclass
 */
abstract class Human
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Please enter a name")
     */
    protected $name;

    /**
     * Set name
     *
     * @param string $name
     * @return Human
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
