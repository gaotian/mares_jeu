<?php

namespace ExerciseBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Troll
 *
 * To extend for the humans knight
 *
 * @package ExerciseBundle\Model
 * @ORM\MappedSuperclass
 */
abstract class Troll
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Please enter a name")
     */
    protected $name;

    //une propriété des trolls qui leur permet d'augmenter leur valeur au combat en regagnant de la vie
    /**
     * @ORM\Column(type="integer")
     */
    protected $regeneration;

    /**
     * Set name
     *
     * @param string $name
     * @return Human
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRegeneration()
    {
        return $this->regeneration;
    }

    /**
     * @param mixed $regeneration
     */
    public function setRegeneration($regeneration)
    {
        $this->regeneration = $regeneration;
    }


}
