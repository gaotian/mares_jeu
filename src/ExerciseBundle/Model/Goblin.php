<?php

namespace ExerciseBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Goblin
 *
 * To extend for the humans knight
 *
 * @package ExerciseBundle\Model
 * @ORM\MappedSuperclass
 */
abstract class Goblin
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Please enter a name")
     */
    protected $name = "Goblin"; //les goblins ont la particularité de tous s'appeler "Goblin" par défaut

    /**
     * Set name
     *
     * @param string $name
     * @return Goblin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
